import { readJson } from "../utils/read-json";
import { Driver } from "./driver";

export class DriversRepository {
  public static instance: DriversRepository;
  private drivers: Driver[] = [];

  constructor() {
    this.getDriverData();
  }

  public static getInstance(): DriversRepository {
    if (!DriversRepository.instance) {
      DriversRepository.instance = new DriversRepository();
    }
    return DriversRepository.instance;
  }

  public getDrivers(): Driver[] {
    return this.drivers;
  }

  public overtake(driverId: string): void {
    const newDriverSequence = [...this.drivers];

    const driverWhoOvertakesIndex = this.drivers.findIndex(driver => driver.id.toString() === driverId);

    if (driverWhoOvertakesIndex === 0) {
      return;
    }

    const driverWhoIsOvertakenIndex = driverWhoOvertakesIndex - 1;
    const driverWhoOvertakes = this.drivers[driverWhoOvertakesIndex];
    const driverWhoIsOvertaken = this.drivers[driverWhoIsOvertakenIndex]

    newDriverSequence[driverWhoIsOvertakenIndex] = { ...driverWhoOvertakes, place: driverWhoOvertakes.place - 1 }
    newDriverSequence[driverWhoOvertakesIndex] = { ...driverWhoIsOvertaken, place: driverWhoIsOvertaken.place + 1 }
    this.drivers = [...newDriverSequence];
  }

  private getDriverData() {
    const jsonData = readJson('src/repository/data/drivers.json');
    if (!jsonData) {
      return;
    }

    const randomizeDrivers = this.randomizeDrivers(JSON.parse(jsonData));
    const driversWithFullData = this.addMissingDataToDrivers(randomizeDrivers);
    this.drivers = driversWithFullData;
  }

  private randomizeDrivers(drivers: Partial<Driver>[]): Partial<Driver>[] {
    // randomize the drivers
    const driverSequence = [...drivers];
    for (let i = driverSequence.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [driverSequence[i], driverSequence[j]] = [driverSequence[j], driverSequence[i]];
    }
    return driverSequence;
  }

  private addMissingDataToDrivers(drivers: Partial<Driver>[]): Driver[] {
    const modifiedDrivers = drivers.map((driver, i) => ({
      ...driver,
      imgUrl: `/static/images/${driver.code?.toLowerCase()}.png`,
      place: i + 1
    }));

    return modifiedDrivers as Driver[];
  }
}