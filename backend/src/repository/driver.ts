export interface Driver {
  id: number;
  code: string;
  firstname: string;
  lastname: string;
  team: string;
  country: string;
  place: number;
  imgUrl: string;
}
