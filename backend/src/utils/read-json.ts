import fs from 'fs';

export const readJson = (path: string): string | void => {
  try {
    return fs.readFileSync(path, 'utf8');

  } catch (err: unknown) {
    console.error((err as Error).message);
  }
}