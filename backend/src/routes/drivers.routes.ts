import express from 'express';
import * as driversControllers from '../controllers/drivers.controller'

export const router = express.Router();

router.get('/drivers', driversControllers.getDrivers)

router.post('/drivers/:driverId/overtake', driversControllers.postDriverOvertake)