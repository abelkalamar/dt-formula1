import { Request, Response } from "express";
import { DriversRepository } from "../repository/DriversRepository";

const driversRepository = DriversRepository.getInstance();

export const getDrivers = (req: Request, res: Response) => {
  const drivers = driversRepository.getDrivers();
  res.status(200).send(drivers);
}

export const postDriverOvertake = (req: Request, res: Response) => {
  const driverId = req.params.driverId;
  driversRepository.overtake(driverId);
  res.status(200).send({ message: `Driver with id ${driverId} overtake` });
}