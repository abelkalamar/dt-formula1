import express from 'express';
import cors from 'cors';
import dotenv from 'dotenv';


import { router as driverRoutes } from './routes/drivers.routes';
import { DriversRepository } from './repository/DriversRepository';

dotenv.config();

const app = express();
const port = process.env.PORT;

// initiate driver repository to get and format driver data
DriversRepository.getInstance();

app.use(cors());
app.use('/static', express.static('public'));

app.use('/api', driverRoutes);

app.listen(port, () => {
  console.log(`⚡️[server]: Server is running at http://localhost:${port}`);
});