import Driver, { DriverModel } from '../components/Driver/Driver';
import classes from './DriversPage.module.scss';
import { getDrivers } from '../utils/get-drivers';
import { useEffect, useState } from 'react';
import { API_BASE_URI } from '../utils/constants';

const DriversPage = () => {
  const [driversData, setDriversData] = useState<DriverModel[]>([]);
  useEffect(() => {
    getDrivers()
      .then((drivers) => {
        setDriversData(drivers);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  const onOvertakeHandler = async (driverId: string) => {
    await fetch(`${API_BASE_URI}/api/drivers/${driverId}/overtake`, {
      method: 'POST',
    });
    const updatedDrivers = await getDrivers();
    setDriversData(updatedDrivers);
  };

  return (
    <div className={classes.content}>
      {driversData.map((driver) => (
        <div key={driver.id} className={classes['driver-card']}>
          <Driver driver={driver} onOvertake={onOvertakeHandler} />
        </div>
      ))}
    </div>
  );
};

export default DriversPage;
