import { useRouteError } from 'react-router-dom';

import classes from './Error.module.scss';

interface RouteError extends Error {
  status: number;
  data: {
    message: string;
  };
}

function ErrorPage() {
  const error: RouteError = useRouteError() as RouteError;
  console.error(error);

  let title = 'An error occurred!';
  let message = 'Something went wrong!';

  if (error.status === 500) {
    message = error.data.message;
  }

  if (error.status === 404) {
    title = 'Not found!';
    message = 'Could not find resource or page.';
  }

  return (
    <>
      <div className={classes.content}>
        <h1>{title}</h1>
        <p>{message}</p>
      </div>
    </>
  );
}

export default ErrorPage;
