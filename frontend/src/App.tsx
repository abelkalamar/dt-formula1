import {
  Navigate,
  RouterProvider,
  createBrowserRouter,
} from 'react-router-dom';
import DriversPage from './pages/DriversPage';
import ErrorPage from './pages/Error';

const router = createBrowserRouter([
  {
    path: '/',
    element: <Navigate to="/drivers" />,
    errorElement: <ErrorPage />,
  },
  {
    path: 'drivers',
    element: <DriversPage />,
  },
]);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
