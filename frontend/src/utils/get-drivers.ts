import { json } from "react-router-dom";
import { API_BASE_URI } from "./constants";

export const getDrivers = async () => {
  const response = await fetch(`${API_BASE_URI}/api/drivers`);

  if (!response.ok) {
    throw json(
      { message: 'Could not fetch the drivers.' },
      {
        status: 500,
      }
    );
  } else {
    return await response.json();
  }
}