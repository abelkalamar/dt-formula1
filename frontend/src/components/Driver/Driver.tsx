import { API_BASE_URI } from '../../utils/constants';
import Card from '../UI/Card';
import ArrowIcon from './ArrowIcon';
import classes from './Driver.module.scss';

export interface DriverModel {
  id: number;
  code: string;
  firstname: string;
  lastname: string;
  team: string;
  country: string;
  place: number;
  imgUrl: string;
}

interface DriverProps {
  driver: DriverModel;
  onOvertake: (id: string) => void; // eslint-disable-line no-unused-vars
}

const Driver: React.FC<DriverProps> = (props) => {
  const { driver } = props;

  const arrowIconClasses = `${classes.icon} ${
    driver.place === 1 ? classes.disabled : ''
  }`;

  const overtakeButtonHandler = () => {
    props.onOvertake(driver.id.toString());
  };

  return (
    <Card>
      <div className={classes.driver}>
        <img
          className={classes.image}
          src={`${API_BASE_URI}${driver.imgUrl}`}
          alt="Driver's image"
        />
        <div className={classes['driver-data']}>
          <h2>{`${driver.firstname} ${driver.lastname}`}</h2>
          <p className={classes.country}>
            Country: <span className={classes.bold}>{driver.country}</span>
            <img
              src={`https://flagsapi.com/${driver.country}/flat/32.png`}
            ></img>
          </p>
          <p>
            Team: <span className={classes.bold}>{driver.team}</span>
          </p>
          <p>
            Code: <span className={classes.bold}>{driver.code}</span>
          </p>
          <p>
            Place: <span className={classes.bold}>{driver.place}</span>
          </p>
        </div>
        <button
          disabled={driver.place === 1}
          className={arrowIconClasses}
          onClick={overtakeButtonHandler}
        >
          <ArrowIcon />
        </button>
      </div>
    </Card>
  );
};

export default Driver;
