# DT test exercise

This is a demo app for the DT interview

## Setup development environment

### Install npm dependencies

- in both the `backend` and `frontend` folders run `npm i` command to install dependencies

### Backend development server

- serve backend (terminal tab 1)

  - run `npm run dev` command in the backend folder

### Web development server

- serve frontend (terminal tab 2)

  - run `npm run start` command in the frontend folder
  - the app will be automatically opened on a new browser tab, but if not visit `http://localhost:3000` url to reach it
